use anyhow::Result;
use rand::seq::SliceRandom;
use rand::thread_rng;
use serenity::model::prelude::Message;
use serenity::prelude::Context;

const GIFS: [&str; 9] = [
    "https://i.gifer.com/7Q9h.gif",
    "https://64.media.tumblr.com/tumblr_m9yhvfF0aG1qc5wono1_500.gif",
    "https://media4.giphy.com/media/PSnqsSkKeyJ9e/giphy.gif",
    "https://media1.tenor.com/images/4924c90c9a728f4cdebea8f71e79d585/tenor.gif?itemid=5119008",
    "https://cdn.discordapp.com/attachments/799900974323073045/800643142206095360/c64.gif",
    "https://tenor.com/view/fullmetalalchemist-fma-armstrong-win-bling-gif-9101886",
    "https://tenor.com/view/manly-handshake-fma-brotherhood-fma-armstrong-armstrong-handshake-gif-16567570",
    "https://tenor.com/view/fma-armstrong-alex-louis-armstrong-fullmetal-alchemist-flex-gif-22210748",
    "https://tenor.com/view/alex-louis-armstrong-fullmetal-alchemist-armstrong-hug-gif-12517445"
];

pub async fn handle_hi(msg: &Message, ctx: &Context) -> Result<(), serenity::Error> {
    if !msg.mentions_me(ctx).await? {
        return Ok(());
    }

    let should_trigger = match msg.content.split_whitespace().nth(1) {
        Some(text) => text == "hi",
        None => false,
    };

    if should_trigger {
        let gif = GIFS.choose(&mut thread_rng()).unwrap();
        msg.reply_ping(&ctx.http, gif).await?;
    }

    Ok(())
}
