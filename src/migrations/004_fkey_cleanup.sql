drop table app_archive_uuids;
create table if not exists app_archive_uuids (
  archive_rowid int unique not null,
  archive_uuid uuid unique not null
);
