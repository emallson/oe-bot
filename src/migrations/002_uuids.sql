create table if not exists app_archive_uuids (
  archive_rowid int not null references app_archive(rowid),
  archive_uuid uuid unique not null
);
