create virtual table if not exists app_archive2 using fts5(
  public_name, 
  private_name,
  public_data,
  private_data,
  tokenize = "trigram"
);

insert into app_archive2 select * from app_archive;

alter table app_archive rename to app_archive_old;
alter table app_archive2 rename to app_archive;
