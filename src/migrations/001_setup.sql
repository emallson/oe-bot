-- Initial setup of data tables for archival
create virtual table if not exists app_archive using fts5(public_name, private_name, public_data, private_data); 
create table if not exists app_archive_dates (archive_rowid int not null, archive_date datetime not null);
