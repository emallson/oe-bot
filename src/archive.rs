use chrono::Utc;
use std::collections::BTreeMap;
use std::iter::Rev;
use std::str::FromStr;
use std::time::Duration;
use uuid::Uuid;

use anyhow::Result;
use poise::futures_util::StreamExt;
use regex::Captures;
use serde::{Deserialize, Serialize};
use serenity::model::prelude::*;
use serenity::prelude::*;

use crate::{BotState, Config};

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("Provided channel is not a text channel in the guild.")]
    InvalidChannel,
    #[error("Unable to find public or private app channel.")]
    MissingAppChannels,
    #[error("Found invalid public/private channel pair.")]
    MismatchedAppChannels,
    #[error("Unable to store data: {0}")]
    StorageError(#[from] sqlx::Error),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ArchiveMessage {
    contents: String,
    timestamp: Timestamp,
    discord_id: MessageId,
    user: UserId,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ArchiveUser {
    discord_id: UserId,
    tag: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ChannelArchive {
    pub name: String,
    messages: Vec<ArchiveMessage>,
    users: BTreeMap<UserId, ArchiveUser>,
}

impl ChannelArchive {
    fn new(channel_name: String) -> Self {
        ChannelArchive {
            name: channel_name,
            messages: vec![],
            users: BTreeMap::new(),
        }
    }

    fn push(&mut self, ctx: &Context, message: Message) {
        let author = message.author.clone();
        self.users.entry(author.id).or_insert_with(|| author.into());
        self.messages.push(ArchiveMessage::from(ctx, message));
    }

    pub fn to_markdown(&self) -> String {
        self.into_iter()
            .map(|(user, msg)| {
                format!(
                    "**{}** ({})<br/>\n{}\n\n",
                    user.tag,
                    msg.timestamp.format("%-I:%M %P, %d %B %Y"),
                    msg.contents
                )
            })
            .collect()
    }

    pub fn to_html(&self) -> String {
        self.into_iter()
            .map(|(user, msg)| {
                format!(
                    "<div><div><strong>{}</strong> ({})</div><p>{}</p></div>",
                    user.tag,
                    msg.timestamp.format("%-I:%M %P, %d %B %Y"),
                    markdown::to_html(&msg.contents)
                )
            })
            .collect()
    }
}

impl<'a> IntoIterator for &'a ChannelArchive {
    type Item = (&'a ArchiveUser, &'a ArchiveMessage);

    type IntoIter = std::iter::Map<
        Rev<core::slice::Iter<'a, ArchiveMessage>>,
        Box<dyn Fn(&'a ArchiveMessage) -> Self::Item + 'a>,
    >;

    fn into_iter(self) -> Self::IntoIter {
        self.messages
            .iter()
            .rev()
            .map(Box::new(|msg| (self.users.get(&msg.user).unwrap(), msg)))
    }
}

impl ArchiveMessage {
    fn from(ctx: &Context, value: Message) -> Self {
        debug!("message contents: {}", &value.content);
        let mut contents = value.content;
        for mention in value.mentions {
            contents =
                contents.replace(&format!("<@{}>", mention.id), &format!("@{}", mention.name));
        }
        for mention in value.mention_roles {
            contents = contents.replace(
                &format!("<@&{}>", mention),
                &format!(
                    "@{}",
                    mention
                        .to_role_cached(ctx)
                        .map(|role| role.name)
                        .unwrap_or("UNKNOWN".to_string())
                ),
            );
        }

        // note: this field is mostly not set
        for mention in value.mention_channels {
            contents =
                contents.replace(&format!("<#{}>", mention.id), &format!("#{}", mention.name));
        }

        let channel_re = regex::Regex::new(r"<#([0-9]+)>").unwrap();
        contents = channel_re
            .replace_all(&contents, |caps: &Captures| {
                let id = ChannelId::from_str(&caps[1]).unwrap();
                id.to_channel_cached(&ctx)
                    .map(|c| {
                        format!(
                            "#{}",
                            match c {
                                Channel::Guild(c) => c.name().to_owned(),
                                Channel::Private(c) => c.name(),
                                Channel::Category(c) => c.name().to_owned(),
                                _ => "Unknown".to_string(),
                            }
                        )
                    })
                    .unwrap_or("#Unknown".to_string())
            })
            .to_string();

        for embed in &value.embeds {
            if let Some(ref title) = embed.title {
                contents += &format!("***{}***\n\n", title);
            }

            for field in &embed.fields {
                contents += &format!("**{}**\n\n{}\n\n", field.name, field.value);
            }
        }

        ArchiveMessage {
            contents,
            discord_id: value.id,
            user: value.author.id,
            timestamp: value.timestamp,
        }
    }
}

impl From<User> for ArchiveUser {
    fn from(value: User) -> Self {
        ArchiveUser {
            discord_id: value.id,
            tag: value.name,
        }
    }
}

pub(crate) async fn archive_channel(
    ctx: &Context,
    channel: ChannelId,
) -> Result<ChannelArchive, super::Error> {
    let mut archive = ChannelArchive::new(channel.name(ctx).await.unwrap());

    let mut msgs = channel.messages_iter(ctx).boxed();
    while let Some(message) = msgs.next().await {
        match message {
            Ok(message) => archive.push(ctx, message),
            Err(error) => {
                return Err(error.into());
            }
        }
    }

    Ok(archive)
}

struct AppChannels {
    public: ChannelId,
    private: ChannelId,
}

struct MaybeAppChannels {
    public: Option<ChannelId>,
    private: Option<ChannelId>,
}

impl MaybeAppChannels {
    fn none() -> Self {
        MaybeAppChannels {
            public: None,
            private: None,
        }
    }

    fn is_none(&self) -> bool {
        self.public.is_none() && self.private.is_none()
    }
}

/// Find the public channel for a private channel or visa-versa.
async fn resolve_app_channels(
    ctx: &Context,
    cfg: &Config,
    id: ChannelId,
) -> Result<Option<AppChannels>, super::Error> {
    let name = id.name(&ctx).await.unwrap();
    let channel = id.to_channel(&ctx).await?;

    let category_id_raw = if let Some(g) = channel.guild() {
        g.parent_id
    } else {
        return Ok(None);
    };

    if category_id_raw != Some(ChannelId(cfg.oew_category))
    {
        return Ok(None);
    }

    let target = if name.ends_with("-private") {
        // private thread, look for public
        name.strip_suffix("-private").unwrap()
    } else {
        name.strip_suffix("-trial").unwrap_or(name.as_str())
    };

    let guild = GuildId(cfg.oe_guild);
    let channels = guild.channels(ctx).await?;

    let mut private_channel = None;
    let mut public_channel = None;
    for channel in channels.into_values() {
        if channel.parent_id != category_id_raw {
            continue;
        }

        if channel.name().starts_with(&target) {
            // match
            if channel.name().ends_with("-private") {
                private_channel = Some(channel.id);
            } else {
                public_channel = Some(channel.id);
            }
        }

        if private_channel.is_some() && public_channel.is_some() {
            break;
        }
    }

    if let (Some(public), Some(private)) = (public_channel, private_channel) {
        if public.name(ctx).await != id.name(ctx).await
            && private.name(ctx).await != id.name(ctx).await
        {
            // mismatched channels, bail
            Err(Error::MismatchedAppChannels.into())
        } else {
            Ok(Some(AppChannels { public, private }))
        }
    } else {
        Err(Error::MissingAppChannels.into())
    }
}

async fn confirm_delete(
    ctx: poise::Context<'_, super::BotState, super::Error>,
    public_channel: Option<ChannelId>,
    private_channel: Option<ChannelId>,
) -> Result<bool, SerenityError> {
    let channel_mentions = [public_channel, private_channel]
        .into_iter()
        .filter_map(|c| c.map(|id| id.mention().to_string()))
        .collect::<Vec<String>>();
    let handle = ctx
        .send(|m| {
            m.ephemeral(true)
                .content(format!(
                    "About to delete {}. This operation cannot be undone. Are you sure?",
                    channel_mentions.join(" and ")
                ))
                .components(|c| {
                    c.create_action_row(|row| {
                        row.create_button(|b| b.label("Delete Channel(s)").custom_id("confirm"))
                            .create_button(|b| {
                                b.label("Cancel")
                                    .custom_id("cancel")
                                    .style(component::ButtonStyle::Danger)
                            })
                    })
                })
        })
        .await?;

    let msg = handle.message().await?;

    let interaction = msg
        .await_component_interaction(ctx)
        .timeout(Duration::from_secs(3600))
        .await;

    handle.delete(ctx).await?;

    if let Some(interaction) = interaction {
        match interaction.data.custom_id.as_str() {
            "confirm" => Ok(true),
            _ => Ok(false),
        }
    } else {
        Ok(false)
    }
}

async fn confirm_app_channels(
    ctx: poise::Context<'_, super::BotState, super::Error>,
    channel: ChannelId,
) -> Result<MaybeAppChannels, super::Error> {
    match resolve_app_channels(ctx.serenity_context(), &ctx.data().config, channel).await {
        Ok(Some(channels)) => Ok(MaybeAppChannels {
            public: Some(channels.public),
            private: Some(channels.private),
        }),
        Ok(None) => Ok(MaybeAppChannels {
            public: None,
            private: Some(channel),
        }),
        Err(super::Error::Archive(Error::MissingAppChannels))
        | Err(super::Error::Archive(Error::MismatchedAppChannels)) => {
            debug!("Found one app channel without the other. Prompting user");
            let response = poise::send_reply(ctx, |reply| {
                reply
                    .content(format!("Unable to locate public/private channel pair. Located channel {}. How do you want to proceed?", channel.mention()))
                    .components(|c| c.create_action_row(|row| {
                            row
                                .create_button(|b| b.label("Only archive this channel").custom_id("single_channel"))
                                .create_button(|b| b.label("Cancel").custom_id("cancel").style(component::ButtonStyle::Danger))
                        }))
            }).await?;

            let mut msg = response.into_message().await?;
            let interaction = msg
                .await_component_interaction(&ctx)
                .timeout(Duration::from_secs(3600))
                .await;

            if let Some(interaction) = interaction {
                msg.edit(ctx, |m| m.components(|c| c)).await?;
                match interaction.data.custom_id.as_str() {
                    "cancel" => {
                        interaction
                            .create_interaction_response(ctx, |r| {
                                r.interaction_response_data(|m| m.content("Operation canceled"))
                            })
                            .await?;
                        return Ok(MaybeAppChannels::none());
                    }
                    "single_channel" => {
                        interaction.defer(ctx).await?;
                        Ok(MaybeAppChannels {
                            public: None,
                            private: Some(channel),
                        })
                    }
                    _ => unreachable!(),
                }
            } else {
                msg.edit(ctx, |m| m.components(|c| c)).await?;
                ctx.say("No response received. Cancelling operation.")
                    .await?;

                Ok(MaybeAppChannels::none())
            }
        }
        Err(err) => Err(err),
    }
}

/// Store an application in the archive
#[poise::command(slash_command, guild_only)]
async fn store(
    ctx: poise::Context<'_, super::BotState, super::Error>,
    #[description = "The channel to archive. Defaults to the current channel."]
    #[channel_types("Text")]
    channel: Option<Channel>,

    #[description = "If true, also delete the channel(s) after archiving. Default false."]
    #[flag]
    delete: bool,
) -> Result<(), super::Error> {
    let channel = if let Some(c) = channel {
        c.id()
    } else {
        ctx.channel_id()
    };

    let channels = confirm_app_channels(ctx, channel).await?;
    if channels.is_none() {
        return Ok(());
    }

    let MaybeAppChannels { public, private } = channels;

    ctx.defer().await?;

    let (public_name, public_archive) = if let Some(channel) = public {
        (
            channel.name(ctx).await,
            Some(archive_channel(ctx.serenity_context(), channel).await?),
        )
    } else {
        (None, None)
    };

    let (private_name, private_archive) = if let Some(channel) = private {
        (
            channel.name(ctx).await,
            Some(archive_channel(ctx.serenity_context(), channel).await?),
        )
    } else {
        (None, None)
    };

    let db = &ctx.data().db;

    let public_json = serde_json::to_string(&public_archive).unwrap();
    let private_json = serde_json::to_string(&private_archive).unwrap();

    let result = sqlx::query!(
        "insert into app_archive (public_name, private_name, public_data, private_data) values (?1, ?2, ?3, ?4)", 
        public_name, 
        private_name, 
        public_json,
        private_json 
    ).execute(db).await.map_err(Error::StorageError)?;

    let rowid = result.last_insert_rowid();

    let archive_date = chrono::offset::Utc::now();
    sqlx::query!(
        "insert into app_archive_dates (archive_rowid, archive_date) values (?1, ?2)",
        rowid, archive_date 
    ).execute(db).await.map_err(Error::StorageError)?;

    let archive_uuid = Uuid::new_v4();

    sqlx::query!(
        "insert into app_archive_uuids (archive_rowid, archive_uuid) values (?1, ?2)",
        rowid, archive_uuid,
    ).execute(db).await.map_err(Error::StorageError)?;

    ctx.say("Application archived.").await?;

    if delete {
        if confirm_delete(ctx, public, private).await? {
            if let Some(channel) = public {
                channel.delete(ctx).await?;
            }

            if let Some(channel) = private {
                channel.delete(ctx).await?;
            }
        } else {
            ctx.send(|m| m.content("Delete operation cancelled.").ephemeral(true))
                .await?;
        }
    }

    Ok(())
}

fn format_autocomplete_name(channel_name: String, date: Option<chrono::DateTime<Utc>>) -> String {
    if let Some(date) = date {
        format!("{} (Archived {})", channel_name, date.format("%d %B %Y"))
    } else {
        channel_name
    }
}

async fn autocomplete_archive(
    ctx: poise::Context<'_, super::BotState, super::Error>,
    partial: &str,
) -> Vec<poise::AutocompleteChoice<u64>> {
    let rows = sqlx::query!(
        r#"
        select app_archive.rowid,
               coalesce(public_name, private_name) as "name?: String",
               archive_date as "archive_date?: chrono::DateTime<Utc>"
        from app_archive 
        join app_archive_dates on app_archive.rowid = app_archive_dates.archive_rowid
        where app_archive match ?1
        order by rank, archive_date desc
        "#,
        partial,
    ).map(|row| match row.rowid {
            Some(rowid) => Some(poise::AutocompleteChoice {
                value: rowid as u64,
                name: format_autocomplete_name(row.name.unwrap_or("Unknown".to_string()), row.archive_date)
            }), None => None })
        .fetch_all(&ctx.data().db)
        .await
        .unwrap_or_default();

    rows.into_iter().filter_map(|v| v).collect()
}

async fn autocomplete_archive_titles(
    ctx: poise::Context<'_, super::BotState, super::Error>,
    partial: &str,
) -> Vec<poise::AutocompleteChoice<u64>> {
    let rows = sqlx::query!(
            r#"
        select app_archive.rowid,
               coalesce(public_name, private_name) as "name?: String",
               archive_date as "archive_date?: chrono::DateTime<Utc>" 
        from app_archive 
        join app_archive_dates on app_archive.rowid = app_archive_dates.archive_rowid
        where app_archive match '{public_name private_name} : ' || ?1
        order by rank, archive_date desc
        "#,
        partial,
    ).map(|row| match row.rowid {
            Some(rowid) => Some(poise::AutocompleteChoice {
                value: rowid as u64,
                name: format_autocomplete_name(row.name.unwrap_or("Unknown".to_string()), row.archive_date)
            }), None => None })
        .fetch_all(&ctx.data().db)
        .await
        .unwrap_or_default();

    rows.into_iter().filter_map(|v| v).collect()
}

async fn archive_link(state: &BotState, app_id: u64) -> Result<String, super::Error> {
    let app_id = app_id as i64;
    let uuid = sqlx::query_scalar!(
            r#"
            select archive_uuid as "uuid: Uuid" 
            from app_archive_uuids 
            where archive_rowid = ?1
            "#,
            app_id,
        ).fetch_optional(&state.db).await
        .map_err(Error::StorageError)?;

    let uuid = match uuid {
        Some(uuid) => uuid,
        None =>{
            let uuid = Uuid::new_v4();
            debug!("no uuid found, inserting {} for app {}", uuid, app_id);
            sqlx::query!(
                r#"
                insert into app_archive_uuids (archive_rowid, archive_uuid)
                values (?1, ?2)
                "#, 
                app_id, 
                uuid
            ).execute(&state.db).await.map_err(Error::StorageError)?;

            uuid
        }
    };

    Ok(format!("{}/archive/{}", state.config.base_url, uuid))
}

/// Search for an app by channel contents
#[poise::command(slash_command, guild_only, rename = "contents")]
async fn search_fulltext(
    ctx: poise::Context<'_, super::BotState, super::Error>,
    #[autocomplete = "autocomplete_archive"]
    #[rename = "query"]
    archive_id: u64,
) -> Result<(), super::Error> {
    ctx.say(archive_link(&ctx.data(), archive_id).await?)
        .await?;

    Ok(())
}

/// Search for an app by channel title.
#[poise::command(slash_command, guild_only, rename = "title")]
async fn search_titles(
    ctx: poise::Context<'_, super::BotState, super::Error>,
    #[autocomplete = "autocomplete_archive_titles"]
    #[rename = "query"]
    archive_id: u64,
) -> Result<(), super::Error> {
    ctx.say(archive_link(&ctx.data(), archive_id).await?)
        .await?;

    Ok(())
}

/// Search for an app. Replies with a link to the archive if found.
#[poise::command(
    slash_command,
    guild_only,
    subcommands("search_titles", "search_fulltext")
)]
async fn search(
    _ctx: poise::Context<'_, super::BotState, super::Error>,
) -> Result<(), super::Error> {
    Ok(())
}

/// Delete application channels.
#[poise::command(slash_command, guild_only)]
async fn delete(
    ctx: poise::Context<'_, super::BotState, super::Error>,

    #[description = "The channel to archive. Defaults to the current channel."]
    #[channel_types("Text")]
    channel: Option<Channel>,

    #[description = "Only delete the selected channel. Do not look for a paired channel."]
    #[flag]
    exact: bool,
) -> Result<(), super::Error> {
    let channel = if let Some(c) = channel {
        c.id()
    } else {
        ctx.channel_id()
    };

    let channels = if exact {
        MaybeAppChannels {
            public: None,
            private: Some(channel),
        }
    } else {
        confirm_app_channels(ctx, channel).await?
    };

    if channels.is_none() {
        return Ok(());
    }

    let MaybeAppChannels { public, private } = channels;
    if confirm_delete(ctx, public, private).await? {
        if let Some(channel) = public {
            channel.delete(ctx).await?;
        }

        if let Some(channel) = private {
            channel.delete(ctx).await?;
        }
    } else {
        ctx.send(|m| m.content("Delete operation cancelled.").ephemeral(true))
            .await?;
    }
    Ok(())
}

/// Store and retrieve applications from the archive.
#[poise::command(
    slash_command,
    guild_only,
    subcommands("search", "store", "delete"),
    default_member_permissions = "MANAGE_GUILD"
)]
pub(crate) async fn archive(
    _ctx: poise::Context<'_, super::BotState, super::Error>,
) -> Result<(), super::Error> {
    Ok(())
}
