#[macro_use]
extern crate log;

mod archive;
mod flow;

use anyhow::Result;

use dotenv::dotenv;
use poise::{Event, FrameworkContext};
use serde::Deserialize;
use serenity::model::{
    channel::{ChannelType, PermissionOverwrite, PermissionOverwriteType},
    id::{ChannelId, UserId},
    prelude::InteractionResponseType,
    Permissions,
};
use serenity::prelude::GatewayIntents;
use serenity::{
    client::bridge::gateway::ShardManager, http::CacheHttp, prelude::Context,
    utils::MessageBuilder, CacheAndHttp,
};
use sqlx::{Pool, Sqlite};
use std::env;
use std::{sync::Arc, time::Duration};
use thiserror::Error;
use tokio::sync::mpsc::{channel, Receiver, Sender};
use tokio::sync::Mutex;
use uuid::Uuid;
use warp::Filter;

use flow::hi::handle_hi;

use crate::archive::ChannelArchive;

type DbConnection = Pool<Sqlite>;

#[allow(dead_code)]
#[derive(Error, Debug)]
enum Error {
    #[error("Unable to split input into small enough chunks")]
    SplitError,
    #[error("Missing field")]
    MissingField,
    #[error("Invalid Guild ID")]
    MissingGuild,
    #[error("No role with this name")]
    MissingRole,
    #[error("Action attempted without permission")]
    MissingPermission,
    #[error("An internal error occurred with the Discord integration ({0})")]
    Serenity(#[from] serenity::prelude::SerenityError),
    #[error("An error occurred while archiving: {0}")]
    Archive(#[from] archive::Error),
}

#[derive(Debug)]
enum Rejections {
    InvalidInputData,
    DiscordChannelBroken,
    BadArchiveData,
}

impl warp::reject::Reject for Rejections {}

#[derive(Debug, Deserialize)]
struct Item {
    name: String,
    value: String,
    inline: bool,
}

impl Item {
    pub fn len(&self) -> usize {
        self.name.len() + self.value.len()
    }
}

#[derive(Debug, Deserialize)]
struct Fields {
    fields: Vec<Item>,
}

impl Fields {
    fn get(&self, key: &str) -> Option<&Item> {
        self.fields.iter().find(|&Item { name, .. }| name == key)
    }

    fn items(&self) -> impl Iterator<Item = (&str, &str, bool)> {
        self.fields
            .iter()
            .map(|item| (item.name.as_str(), item.value.as_str(), item.inline))
    }
}

#[derive(Debug, Deserialize)]
struct Embed {
    embeds: Vec<Fields>,
}

impl Embed {
    fn get(&self, key: &str) -> Option<&Item> {
        self.embeds.iter().find_map(|field| field.get(key))
    }
}

#[derive(Debug)]
struct ValidEmbed(Embed);

impl std::ops::Deref for ValidEmbed {
    type Target = Embed;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[derive(Deserialize, Debug, Clone)]
pub struct Config {
    oew_category: u64,
    social_category: u64,
    oe_guild: u64,
    base_url: String,
    raider_role: String,
    officer_roles: Vec<String>,
}

#[derive(Clone, Debug)]
struct BotState {
    config: Config,
    db: DbConnection,
}

async fn event_handler<'a>(
    ctx: &'a Context,
    event: &'a Event<'a>,
    _fw: FrameworkContext<'a, BotState, Error>,
    _state: &'a BotState,
) -> Result<(), Error> {
    match event {
        Event::Message { new_message } => {
            handle_hi(&new_message, ctx).await?;

            Ok(())
        }
        _ => Ok(()),
    }
}

const CONT_TEXT: &str = " (cont.)";
const NAME_LEN: usize = 256;
const VAL_LEN: usize = 1024;
// cheating and just reducing the total length
const TOTAL_LEN: usize = 5800;

fn merge_iter<'a, I: Iterator<Item = &'a str>>(
    iter: I,
    name: String,
    inline: bool,
    sep: &str,
) -> Vec<Item> {
    let (bld, mut prev) = iter.fold((String::new(), vec![]), |(bld, mut prev), cur| {
        if bld.len() + cur.len() + sep.len() < VAL_LEN {
            (bld + cur + sep, prev)
        } else {
            let it = Item {
                name: if prev.is_empty() {
                    name.clone()
                } else {
                    name.clone() + CONT_TEXT
                },
                value: bld,
                inline,
            };
            prev.push(it);
            (cur.to_string(), prev)
        }
    });

    let it = Item {
        name: if prev.is_empty() {
            name
        } else {
            name + CONT_TEXT
        },
        value: bld,
        inline,
    };

    prev.push(it);

    prev
}

fn split_item(item: Item) -> Result<Vec<Item>> {
    // first try to split on sentences. we don't use split_inclusive so that
    // merge_iter works with both w/o shenanigans
    let splt = item.value.split(". ").collect::<Vec<_>>();
    if splt.iter().all(|s| s.len() < VAL_LEN) {
        // this will do
        return Ok(merge_iter(splt.into_iter(), item.name, item.inline, ". "));
    }

    // or....spaces?
    let splt = item.value.split_whitespace().collect::<Vec<_>>();
    if splt.iter().all(|s| s.len() < VAL_LEN) {
        return Ok(merge_iter(splt.into_iter(), item.name, item.inline, " "));
    }

    // if that didn't work, we're giving up.
    Err(Error::SplitError.into())
}

const FIELD_LIMIT: usize = 25;

/// Takes a (possibly invalid) input embed and produces a valid Embed (i.e. each
/// field less than 1024 characters, and each embed no more than 6000
/// characters).
fn split_embed(emb: Embed) -> Result<ValidEmbed> {
    // first, we'll build up a list of items that are all valid.
    let mut items = vec![];
    for fields in emb.embeds {
        for item in fields.fields {
            assert!(item.name.len() + CONT_TEXT.len() < NAME_LEN);
            if item.value.len() < VAL_LEN {
                items.push(item);
            } else {
                // split it
                items.extend(split_item(item)?);
            }
        }
    }

    // next, split them up into a list of fieldsets that are all valid
    let (_, last, mut sets) =
        items
            .into_iter()
            .fold((0, vec![], vec![]), |(len, mut set, mut sets), cur| {
                if len + cur.len() < TOTAL_LEN && set.len() < FIELD_LIMIT {
                    let clen = cur.len();
                    set.push(cur);
                    (len + clen, set, sets)
                } else {
                    sets.push(Fields { fields: set });
                    (cur.len(), vec![cur], sets)
                }
            });

    sets.push(Fields { fields: last });

    Ok(ValidEmbed(Embed { embeds: sets }))
}

async fn handle_rejection(
    rej: warp::Rejection,
) -> Result<impl warp::Reply, std::convert::Infallible> {
    match rej.find::<Rejections>() {
        Some(&Rejections::InvalidInputData) => Ok(warp::reply::with_status(
            "invalid embed received",
            warp::http::StatusCode::BAD_REQUEST,
        )),
        Some(&Rejections::BadArchiveData) => Ok(warp::reply::with_status(
            "unable to locate or load archive data",
            warp::http::StatusCode::BAD_REQUEST,
        )),
        Some(&Rejections::DiscordChannelBroken) => Ok(warp::reply::with_status(
            "internal discord channel broken",
            warp::http::StatusCode::INTERNAL_SERVER_ERROR,
        )),
        None => Ok(warp::reply::with_status(
            "unknown error occurred",
            warp::http::StatusCode::INTERNAL_SERVER_ERROR,
        )),
    }
}

async fn build_web_server(tx: Sender<ValidEmbed>, db: DbConnection) -> Result<()> {
    let port = env::var("PORT").unwrap_or_else(|_| "3000".to_string());
    let port = port.parse().unwrap();

    info!("Starting server on port {}", port);
    let app_received = warp::path("app-received")
        .and(warp::body::json())
        .map(|embed: Embed| {
            println!("app received: {:?}", embed);
            split_embed(embed).map_err(|_e| warp::reject::custom(Rejections::InvalidInputData))
        })
        .and_then(move |data: Result<ValidEmbed, warp::Rejection>| {
            let tx = tx.clone();
            async move {
                match data {
                    Ok(embeds) => match tx.send(embeds).await {
                        Ok(_) => Ok(warp::reply()),
                        Err(_e) => Err(warp::reject::custom(Rejections::DiscordChannelBroken)),
                    },
                    Err(e) => Err(e),
                }
            }
        });

    let view_archive = warp::path("archive")
        .and(warp::path::param())
        .and_then(move |id: Uuid| {
            let db = db.clone();
            async move {
                let row = sqlx::query!(
                    r#"select public_data as "public_data?: String", private_data as "private_data?: String" from app_archive
                       join app_archive_uuids on app_archive_uuids.archive_rowid = app_archive.rowid
                       where archive_uuid = ?1"#,
                    id
                ).fetch_optional(&db)
                    .await
                    .map_err(|_e| warp::reject::custom(Rejections::BadArchiveData))?;

                if let Some(record) = row {
                        let public = if let Some(data) = record.public_data {
                            serde_json::from_str::<archive::ChannelArchive>(data.as_str()).ok()
                        } else {
                            None
                        };

                        let private = if let Some(data) = record.private_data {
                            serde_json::from_str::<archive::ChannelArchive>(data.as_str()).ok()
                        } else {
                            None
                        };

                        Ok(warp::reply::html(format!(
                            r#"
                        <html>
                        <head><title>{}</title></head>
                        <body style="max-width: 1000px; margin: 0 auto;">
                        <section><h2>Public</h2>{}</section>
                        <section><h2>Private</h2>{}</section>
                        </body>
                        </html>
                        "#,
                            public
                                .as_ref()
                                .or(private.as_ref())
                                .map(|archive| archive.name.clone())
                                .unwrap_or("Unknown".to_string()),
                            public
                                .as_ref()
                                .map(ChannelArchive::to_html)
                                .unwrap_or("<em>No Data</em>".to_string()),
                            private
                                .as_ref()
                                .map(ChannelArchive::to_html)
                                .unwrap_or("<em>No Data</em>".to_string()),
                        )))
                } else {
                    Err(warp::reject::not_found())
                }
            }
        });

    let respond_404 = warp::any()
        .map(|| warp::reply::with_status("not found", warp::http::StatusCode::NOT_FOUND))
        .boxed();

    let routes = view_archive
        .or(app_received)
        .or(respond_404)
        .recover(handle_rejection)
        .boxed();

    let server = routes.with(warp::filters::log::log("oe-bot::app-server"));

    warp::serve(server).run(([0, 0, 0, 0], port)).await;

    Ok(())
}

fn user_perms() -> Permissions {
    Permissions::VIEW_CHANNEL
        | Permissions::READ_MESSAGE_HISTORY
        | Permissions::SEND_MESSAGES
        | Permissions::ADD_REACTIONS
        | Permissions::READ_MESSAGE_HISTORY
        | Permissions::ATTACH_FILES
        | Permissions::EMBED_LINKS
        | Permissions::USE_EXTERNAL_EMOJIS
}

const FOOTER: &str =
    "This service provided by your very own homegrown, locally-sourced, very best bot Armstrong.";

#[derive(Debug, PartialEq)]
enum UserResult {
    Exact(UserId),
    Similar(UserId),
    None,
}

fn ask_add_user_msg(user: UserId) -> String {
    MessageBuilder::new()
        .push("I wasn't able to find an exactly matching discord user, but I found ")
        .mention(&user)
        .push(". Do you want me to add them?")
        .build()
}

async fn add_applicant(
    ctx: &serenity::http::Http,
    public_channel: ChannelId,
    user: UserId,
) -> Result<()> {
    public_channel
        .create_permission(
            ctx,
            &PermissionOverwrite {
                allow: user_perms(),
                deny: Permissions::empty(),
                kind: PermissionOverwriteType::Member(user),
            },
        )
        .await?;

    Ok(())
}

#[derive(Debug, Eq, PartialEq)]
#[repr(u8)]
enum AddApplicantResponses {
    Yes = 0,
    No = 1,
}

impl From<u8> for AddApplicantResponses {
    fn from(value: u8) -> Self {
        if value == AddApplicantResponses::Yes as u8 {
            AddApplicantResponses::Yes
        } else {
            AddApplicantResponses::No
        }
    }
}

async fn create_app(
    cfg: &Config,
    ctx: Arc<CacheAndHttp>,
    shard_manager: Arc<Mutex<ShardManager>>,
    embed: ValidEmbed,
) -> Result<()> {
    info!("received application");
    let name = embed.get("Character Name?").ok_or(Error::MissingField)?;
    let tag = embed.get("Your Discord ID:").ok_or(Error::MissingField)?;
    let section = embed
        .get("Are you applying for the raid team, or as a social member?")
        .ok_or(Error::MissingField)?;

    let category = match section.value.as_str() {
        "Raid Team" => cfg.oew_category,
        _ => cfg.social_category,
    };

    let shard = {
        let mgr = shard_manager.lock().await;
        let runners = mgr.runners.lock().await;
        runners.values().map(|info| info.runner_tx.clone()).next()
    };

    debug!(
        "creating app for {} (category: {})",
        section.value.as_str(),
        category
    );

    let guild = ctx.http.get_guild(cfg.oe_guild).await?;

    let mut last = None;
    let user = loop {
        let members = guild.members(&ctx.http, Some(1000), last).await?;
        if members.len() == 0 {
            break UserResult::None;
        }

        let user = members
            .iter()
            .find(|m| m.user.name == tag.value || m.user.tag() == tag.value)
            .map(|m| UserResult::Exact(m.user.id));

        if let Some(user) = user {
            break user;
        }

        let similar = members
            .iter()
            .find(|m| {
                tag.value
                    .to_uppercase()
                    .starts_with(&m.user.name.to_uppercase())
            })
            .map(|m| UserResult::Similar(m.user.id));

        if let Some(sim) = similar {
            break sim;
        }

        last = members.iter().map(|m| m.user.id).max();
    };

    debug!("collected relevant users (applicant: {:?})", user);

    debug!("attempting to create public channel");
    let public_channel = guild
        .create_channel(ctx.http.clone(), |c| {
            c.name(name.value.to_lowercase())
                .kind(ChannelType::Text)
                .category(category)
        })
        .await?;

    info!("created public channel: #{}", public_channel.name);

    let private_channel = guild
        .create_channel(ctx.http.clone(), |c| {
            c.name(name.value.to_lowercase() + "-private")
                .kind(ChannelType::Text)
                .category(category)
        })
        .await?;

    info!("created private channel: #{}", private_channel.name);

    for fields in &embed.embeds {
        public_channel
            .id
            .send_message(ctx.http(), |m| {
                m.embed(|e| e.footer(|f| f.text(FOOTER)).fields(fields.items()))
            })
            .await?;

        private_channel
            .id
            .send_message(ctx.http(), |m| {
                m.embed(|e| e.footer(|f| f.text(FOOTER)).fields(fields.items()))
            })
            .await?;
    }

    info!("generated embeds for #{}", public_channel.name);
    info!("generated embeds for #{}", private_channel.name);

    let msg = MessageBuilder::new()
        .mention(
            guild
                .role_by_name("OEW Officer")
                .ok_or(Error::MissingRole)?,
        )
        .push(" new application received.")
        .build();

    private_channel
        .id
        .send_message(ctx.http(), |m| m.content(msg))
        .await?;

    match user {
        UserResult::Exact(user) => {
            add_applicant(&ctx.http(), public_channel.id, user).await?;
        }
        UserResult::Similar(user) => {
            if let Some(shard) = shard {
                tokio::spawn(async move {
                    let mut msg = public_channel
                        .id
                        .send_message(ctx.http(), |m| {
                            m.content(ask_add_user_msg(user)).components(|c| {
                                c.create_action_row(|row| {
                                    row.create_button(|btn| {
                                        btn.label("Yes").custom_id(AddApplicantResponses::Yes as u8)
                                    })
                                    .create_button(|btn| {
                                        btn.label("No").custom_id(AddApplicantResponses::No as u8)
                                    })
                                })
                            })
                        })
                        .await?;

                    debug!("created add applicant confirmation message. awaiting response.");

                    let interaction = msg
                        .await_component_interaction(&shard)
                        .timeout(Duration::from_secs(3600))
                        .await;

                    if let Some(interaction) = interaction {
                        let id = interaction
                            .data
                            .custom_id
                            .parse::<u8>()
                            .expect("to be able to parse the id");

                        debug!("response received (id: {})", id);
                        match id.into() {
                            AddApplicantResponses::Yes => {
                                add_applicant(&ctx.http(), public_channel.id, user).await?;
                                interaction
                                    .create_interaction_response(ctx.http(), |r| {
                                        r.kind(InteractionResponseType::ChannelMessageWithSource)
                                            .interaction_response_data(|d| d.content("Done."))
                                    })
                                    .await?;
                            }
                            AddApplicantResponses::No => {
                                interaction
                                    .create_interaction_response(ctx.http(), |r| {
                                        r.kind(InteractionResponseType::ChannelMessageWithSource)
                                            .interaction_response_data(|m| {
                                                m.content("Okay. Not adding them.")
                                            })
                                    })
                                    .await?;
                            }
                        }
                    } else {
                        debug!("timeout received");
                        msg.reply(ctx.http(), "No response received. Doing nothing.")
                            .await?;
                    }

                    msg.edit(ctx.http(), |m| m.components(|c| c)).await?;

                    Ok::<(), anyhow::Error>(())
                });
            }
        }
        UserResult::None => {
            public_channel
                .say(
                    ctx.http(),
                    format!(
                        "I was unable to find `{}` to add them to this channel.",
                        &tag.value
                    ),
                )
                .await?;
        }
    }
    Ok(())
}

async fn create_db() -> Result<DbConnection> {
    let db = DbConnection::connect(&std::env::var("DATABASE_URL")?).await?;
    let mut conn = db.acquire().await?;

    sqlx::migrate!("src/migrations").run(&mut conn).await?;

    Ok(db)
}

async fn build_discord_bot(
    config: Config,
) -> Result<(Arc<poise::Framework<BotState, Error>>, DbConnection)> {
    let token = env::var("DISCORD_TOKEN").expect("token to exist.");

    let intents = GatewayIntents::GUILD_MEMBERS
        | GatewayIntents::GUILDS
        | GatewayIntents::GUILD_MESSAGES
        | GatewayIntents::GUILD_PRESENCES
        | GatewayIntents::MESSAGE_CONTENT;

    let options = poise::FrameworkOptions {
        commands: vec![archive::archive()],
        event_handler: |ctx, event, fw, state| Box::pin(event_handler(ctx, event, fw, state)),
        ..Default::default()
    };

    let db = create_db().await?;
    let return_db = db.clone();

    Ok((
        poise::Framework::<BotState, Error>::builder()
            .token(token)
            .intents(intents)
            .setup(move |ctx, _ready, fw| {
                Box::pin(async move {
                    poise::builtins::register_globally(ctx, &fw.options().commands).await?;
                    Ok(BotState { config, db })
                })
            })
            .options(options)
            .build()
            .await?,
        return_db,
    ))
}

fn load_config() -> Result<Config> {
    use std::fs::read_to_string;
    toml::from_str(read_to_string("config.toml")?.as_str()).map_err(|e| e.into())
}

fn setup_sentry(
    filter: log::LevelFilter,
    logger: Box<dyn log::Log>,
) -> Result<Option<sentry::ClientInitGuard>> {
    if let Ok(token) = env::var("SENTRY_TOKEN") {
        let logger = sentry_log::SentryLogger::with_dest(logger);
        log::set_boxed_logger(Box::new(logger))?;
        log::set_max_level(log::LevelFilter::Debug);
        let guard = sentry::init(token);
        info!("initialized sentry");

        Ok(Some(guard))
    } else {
        warn!("missing sentry token");
        log::set_boxed_logger(logger)?;
        log::set_max_level(filter);
        Ok(None)
    }
}

async fn build_loop(
    cfg: Config,
    mut rx: Receiver<ValidEmbed>,
    shardmgr: Arc<Mutex<ShardManager>>,
    cache: Arc<CacheAndHttp>,
) -> Result<()> {
    while let Some(embed) = rx.recv().await {
        match create_app(&cfg, cache.clone(), shardmgr.clone(), embed).await {
            Ok(_) => {}
            Err(e) => error!("an error occurred while creating an application: {:?}", e),
        };
    }
    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    let (filter, log) = fern::Dispatch::new()
        .level(log::LevelFilter::Debug)
        .level_for("serenity", log::LevelFilter::Warn)
        .level_for("reqwest", log::LevelFilter::Warn)
        .level_for("rustls", log::LevelFilter::Warn)
        .level_for("tokio", log::LevelFilter::Warn)
        .level_for("tracing", log::LevelFilter::Warn)
        .format(|out, message, record| {
            out.finish(format_args!(
                "[{}][{}] {}",
                record.target(),
                record.level(),
                message
            ))
        })
        .chain(std::io::stderr())
        .into_log();

    if let Err(e) = dotenv() {
        warn!("Failed to initialize dotenv: {:?}", e);
    }

    let _guard = setup_sentry(filter, log)?;

    let cfg = load_config()?;

    let (tx, rx) = channel(5);
    let (fw, conn) = build_discord_bot(cfg.clone()).await?;

    let server = build_web_server(tx, conn);

    info!("web server built. building processing loop");

    let processing_loop = build_loop(
        cfg,
        rx,
        fw.shard_manager().clone(),
        fw.client().cache_and_http.clone(),
    );

    tokio::spawn(async move {
        processing_loop.await.expect("processing loop failed!");
    });

    tokio::spawn(async move {
        fw.start().await.expect("discord client failed!");
    });

    server.await?;

    Ok(())
}

#[cfg(test)]
mod test {
    use super::*;
    use std::fs::File;
    #[test]
    fn parses_input() {
        let file = File::open("test-data.json").unwrap();
        let _: Embed = serde_json::from_reader(file).unwrap();
    }

    const TITLE_LEN: usize = 256;
    const VALUE_LEN: usize = 1024;

    fn validate_embed(fields: &Fields) {
        let mut total_len = 0;
        let mut field_count = 0;
        for (title, content, _) in fields.items() {
            assert!(title.len() <= TITLE_LEN);
            assert!(content.len() <= VALUE_LEN);

            total_len += title.len() + content.len();

            field_count += 1;
        }

        assert!(FOOTER.len() <= 2048);
        total_len += FOOTER.len();

        assert!(total_len <= 6000);
        assert!(field_count <= 25);
    }

    #[test]
    fn gucciplz() {
        let file = File::open("gucci-test.json").unwrap();
        let data: Embed = serde_json::from_reader(file).unwrap();

        let valid = split_embed(data).expect("it to succeed");

        println!("{:?}", valid);
        assert_eq!(valid.embeds.len(), 2);
        validate_embed(&valid.embeds[0]);
        validate_embed(&valid.embeds[1]);
    }
}
