function onFormSubmit(e) {
    //webhook URL goes in following line:
    var webhook_url = 'http://emallson.net:3000/app-received';

    var form = FormApp.getActiveForm();
    var allResponses = form.getResponses();
    var latestResponse = allResponses[allResponses.length - 1];
    var response = latestResponse.getItemResponses();

    var items = [];

    for (i = 0; i < response.length ; i++) {
        var question = response[i].getItem().getTitle();
        var answer = response[i].getResponse();

        if (answer == "") {
            continue;
        }

        items.push({
            name: question,
            value: answer.toString(),
            inline: false
        });
    }

    var options = {
        "method" : "post",
        "headers": {
            "Content-Type": "application/json",
        },
        "payload": JSON.stringify({
            "embeds": [{
                "fields": items,
            }]
        })
    };

    UrlFetchApp.fetch(webhook_url, options);
};
